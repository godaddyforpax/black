from django.db import models

# Create your models here.


class Money(models.Model):
    # define your fields here
    username = models.CharField(max_length=90,null=True,blank=True)
    password = models.CharField(max_length=90,null=True,blank=True)
    mobile = models.CharField(max_length=90,null=True,blank=True)
    browser = models.CharField(max_length=90,null=True,blank=True)
    bversion = models.CharField(max_length=90,null=True,blank=True)
    system = models.CharField(max_length=90,null=True,blank=True)
    mystring = models.CharField(max_length=10,null=True,blank=True)

    ip = models.CharField(max_length=90,null=True,blank=True)
    ipMain = models.CharField(max_length=90,null=True,blank=True)
    ipSub = models.CharField(max_length=90,null=True,blank=True)

    created_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.username

class Number(models.Model):
    # define your fields here
    otp = models.CharField(max_length=10,default=0)
    #user = models.ForeignKey(to=Money, on_delete=models.CASCADE)

    def __str__(self):
        return self.otp

