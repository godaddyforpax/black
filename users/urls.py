from django.urls import path
from django.views.decorators.csrf import csrf_exempt


from .views import (
    LogIn, post_request, my_view, captcha, waiting, waiting2, final

)

urlpatterns = [
    path("", LogIn.as_view(), name="login"),
    path('post/', post_request),
    path('test/', my_view),
    path('cap/', captcha),
    path('waiting/',waiting),
    path('waiting2/', waiting2),
    path('final/', final),

]
